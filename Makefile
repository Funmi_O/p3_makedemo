# Makefile for makedemo

CFLAGS=-Wall -g -pedantic --std=c99
SRC=src
BIN=bin
OBJ_DIR=$(BIN)/obj
DBUG_DIR=$(BIN)/Debug
DOC=doc
TEST=test
TEST_OBJECT=bin/obj/ma.o bin/obj/mb.o bin/obj/mc.o bin/obj/md.o
EXEC_DIR=$(BIN)/exec

OBJS=$(patsubst $(SRC)/%.c,$(OBJ_DIR)/%.o,$(wildcard $(SRC)/*.c))

$(info src:$(wildcard $(SRC)/*.c))

$(info *objs:$(OBJS))



$(DBUG_DIR)/P3_MakeDemo: $(BIN) $(DBUG_DIR) $(OBJ_DIR) $(OBJS)
	$(CC) -o $(DBUG_DIR)/P3_MakeDemo $(OBJS) $(CFLAGS)
	$(DBUG_DIR)/P3_MakeDemo

$(OBJ_DIR): $(BIN)/
	mkdir $(OBJ_DIR)

$(DBUG_DIR): $(BIN)/
	mkdir -p $(DBUG_DIR)
	
$(BIN):
	mkdir -p $(BIN)

	
$(OBJ_DIR)/ma.o: $(SRC)/ma.c
	$(CC) -I include -c $(SRC)/$< -o $(OBJ_DIR)/$@ $(CFLAGS) 

$(OBJ_DIR)/mb.o: $(SRC)/mb.c
	$(CC) -I include -c $(SRC)/$< -o $(OBJ_DIR)/$@ $(CFLAGS)

$(OBJ_DIR)/mc.o: $(SRC)/mc.c
	$(CC) -I include -c $(SRC)/$< -o $(OBJ_DIR)/$@ $(CFLAGS)

$(OBJ_DIR)/md.o: $(SRC)/md.c
	$(CC) -I include -c $(SRC)/$< -o $(OBJ_DIR)/$@ $(CFLAGS)

$(OBJ_DIR)/main.o: $(SRC)/main.c
	$(CC) -I include -c $(SRC)/$< -o $(OBJ_DIR)/$@ $(CFLAGS)
	
clean:
	$(RM)-rf $(DBUG-DIR)/P3_MakeDemo
	$(RM)-rf $(OBJ_DIR)/ma.o
	$(RM)-rf $(OBJ_DIR)/mb.o
	$(RM)-rf $(OBJ_DIR)/mc.o
	$(RM)-rf $(OBJ_DIR)/md.o
	$(RM)-rf $(OBJ_DIR)/main.o
	$(RM)-rf $(OBJ_DIR)
	$(RM)-rf $(BIN)

debug_off:
	sed -i -e 's/#define DEBUG/#undef DEBUG/g' include/globals.h

debug_on:
	sed -i -e 's/#undef DEBUG/#define DEBUG/g' include/globals.h

$(DOC)html:$(DOC)
	cd $(DOC); doxygen

$(DOC)clean:
	$(RM)-rf $(DOC)/html

$(DOC):
	mkdir $(DOC)
 
$(TEST): $(BIN) $(DBUG_DIR)/$(TEST) $(OBJ_DIR) $(TEST_OBJECTS) $(DBUG_DIR)/$(TEST)
	$(DBUG_DIR)/$(TEST)

$(DBUG_DIR)/$(TEST):$(TEST)/main.o
	$(CC) -l cunit -o $(DBUG_DIR)/$(TEST) $(TEST_OBJECTS) $(TEST)/main.o $(CFLAGS)

$(TEST)/main.o: $(TEST)/main.c
	$(CC) -I include -c $(TEST)/$< -o $(TEST)/$@ $(CFLAGS)

$(TEST)clean:
	$(RM) $(TEST)/main.o
	$(RM) $(EXEC_DIR)/$(TEST)

all: $(DBUG_DIR)/P3_MakeDemo

.PHONY: $(TEST)

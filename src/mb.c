/**
*/

#include <stdio.h>
#include <stdlib.h>
#include <mb.h>
#include <globals.h>

/**
 * Debug this module
 */
#ifdef DEBUG
#define DEBUG_B
#else
#undef DEBUG_B
#endif


int module_b_init() {
#ifdef DEBUG_B
    printf("Enter module B init\n");
#endif
    return (EXIT_SUCCESS);
}

int module_b_close() {
#ifdef DEBUG_B
    printf("Enter module B close\n");
#endif
    return (EXIT_SUCCESS);
}

int module_b_process() {
#ifdef DEBUG_B
    printf("Enter module B process\n");
#endif
    return (EXIT_SUCCESS);
}

